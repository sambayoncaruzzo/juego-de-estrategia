package domain_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sambayoncaruzzo/juego-de-estrategia/src/internal/domain"
)

type startBattleInput struct {
	Attacker  domain.AttackerDTO
	Defender domain.DefenderDTO
	Map      domain.MapDTO
}

func TestStartBattle(t *testing.T) {
	var tests = []struct {
		name     string
		expected domain.BattleDTO
		given    startBattleInput
	}{
		{
			name: "StartBattle",
			expected: domain.BattleDTO{
				Status: "Ready",
			},
			given: startBattleInput{
				Attacker: domain.AttackerDTO{
					Player: domain.PlayerDTO{},
					Army:   domain.ArmyDTO{},
				},
				Defender: domain.DefenderDTO{
					Player: domain.PlayerDTO{},
					Army:   domain.ArmyDTO{},
				},
				Map: domain.MapDTO{
					Name:       "MapName",
					DeployZone: []domain.CellDTO{},
				},
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			attackerInput := tt.given.Attacker
			defenderInput := tt.given.Defender
			mapInput := tt.given.Map
			result, _ := domain.StartBattle(attackerInput, defenderInput, mapInput)
			assert.Equal(t, tt.expected, result)

		})
	} // t.Fatal("not implemented")
}
