package domain

type DefenderDTO struct{
	Player PlayerDTO
	Army ArmyDTO
}