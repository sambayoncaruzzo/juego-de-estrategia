package domain

type AttackerDTO struct{
	Player PlayerDTO
	Army ArmyDTO
}